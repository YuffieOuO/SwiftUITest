//
//  ContentView.swift
//  SwiftUITest
//
//  Created by Yuffie on 2022/3/7.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @State var model: [ButtonModel]

    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
    ]
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: columns) {
                ForEach(model.indices) { index in
                    Button(model[index].text) {
                        model[index].isSelected.toggle()

                    }
                    .frame(width: 50, height: 35)
                    .foregroundColor(model[index].isSelected ? .white : .black)
                    .background(model[index].isSelected ? .cyan : .yellow)
                    .cornerRadius(.infinity)
                }
            }
        }
    }
}

func appendData() -> [ButtonModel] {
    var model = [ButtonModel]()
    for i in 1...49 {
        let text = String(format: "%02d", i)
        model.append(.init(text: text))
    }
    return model
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(model: appendData())
    }
}
