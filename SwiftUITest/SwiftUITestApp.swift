//
//  SwiftUITestApp.swift
//  SwiftUITest
//
//  Created by Yuffie on 2022/3/7.
//

import SwiftUI

@main
struct SwiftUITestApp: App {
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            ContentView(model: appendData())
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
