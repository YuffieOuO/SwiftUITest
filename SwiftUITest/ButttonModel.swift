//
//  ButttonModel.swift
//  SwiftUITest
//
//  Created by Yuffie on 2022/3/17.
//

import Foundation

struct ButtonModel: Identifiable {
    var id = UUID()
    var isSelected = false
    var text: String
}
